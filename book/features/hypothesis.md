# Hypothesis

Add this to `_config.yml`. Need to create an account.


```
html:
  comments:
    hypothesis: true
```

```{admonition} Tips
:class: tip

- use {ref}`remove_from_publish` to enable in Draft book only
- figure out how to use this...
```


