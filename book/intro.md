# Jupyter Book Manual

This is an introduction to Git, VS Code and Jupyter Books for teachers and teaching assistants involved in TeachBooks. This book is still work in progress, so feel free to contribute https://gitlab.tudelft.nl/interactivetextbooks-citg/jupyter-book-manual! For more information about teachbooks, visit [https://teachbooks.tudelft.nl/](https://teachbooks.tudelft.nl/).

## Contact
If you encouter any issues, please contact Tom van Woudenberg, Robert Lanzafame or one of our TAs Caspar Jungbacker or Julie Kirsch at TeachBooks@tudelft.nl.
