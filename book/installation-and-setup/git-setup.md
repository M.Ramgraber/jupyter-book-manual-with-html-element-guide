# What is Git and GitLab?

In this section you will learn how to perform basic Git actions in the online interface of GitLab.

## What is Git?

Git is a version control system (VCS), used by a wide variety of engineers and software developers to work on projects in parallel together. It provides multiple benefits such as tracking changes to files, working side by side with other people, and the ability to rollback to previous versions of files without losing track of newer changes. It is a free and open sources software.

## What is GitLab?

GitLab is a cloud-based version control system built around git. It provides a lot more features such as Issues, Merge Requests, CI/CD pipelines, etc. TU Delft has a license to use GitLab on our own local webservers—this means that all of the files are stored digitally on the TU Delft campus, rather than some unkown webserver that could be physically located in an unsafe location. This is also why we have our “own” GitLab located at gitlab.tudelft.nl, rather than the “normal” GitLab at gitlab.com.

## Terminology
This is a (non-exhaustive) list of terminology that will be used in GitLab.

- Repository: a collection of files and folders, along with a history of their changes and who made them. 
- Commit: a snapshot of the current state of the repository.
- Branch: a development line.
- Pushing: uploading new commits to the remote repository.