# Making comments on the website

This section will explain how to use the extension in order to make comments.

Firstly, you will need to open your interactive text book in your browser. Take as example the [MUDE](https://mude.citg.tudelft.nl/book/intro.html) book.

If you look at the top right of the page, you will see three icons, an arrow, an eye and a note-pad. Clicking on the arrow will open the extension. The extension we are currently using is called Hypothesis. In order to use it you will have to make an account. Through the extension you will be able to highlight relevant sections and write notes and annotations. Comments on the content of the book can be submitted by making annotations and posting them to group called Feedback!
