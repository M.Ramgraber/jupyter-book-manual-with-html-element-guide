# Workflows

```{warning} Under Construction!

This page will provide a workflow how to collaborate in Git and GitLab for different uses types. Every team might have a different workflow but this workflow is shown as an example and is used in the MUDE course in CEG.

An alternative is to use Github and Github pages to publish your book.