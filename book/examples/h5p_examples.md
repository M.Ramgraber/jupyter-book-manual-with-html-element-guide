# H5p

This page shows some examples of H5p questions.

Some best-practices on the use of H5p:
 - To startup your H5p-account, first login to H5p via Brightspace (follow instructions as under [https://www.tudelft.nl/teaching-support/educational-tools/h5p](https://www.tudelft.nl/teaching-support/educational-tools/h5p)). After that, direct login is possible via [tudelft.h5p.com](tudelft.h5p.com)
 - Place your H5p-elements in a shared folder in H5p.
 - Disable the display options "Toolbar Below Content" and "Display author's name to public (anonymous users) (only relevant when content's status is set to Public )" for each 

## Complex fill in the blanks
This can be used to evaluate numerical answers:
<iframe src="https://tudelft.h5p.com/content/1292010272694757307/embed" aria-label="Example MUDE" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://tudelft.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script>

## Dialog cards
This can be used to ask open questions:
<iframe src="https://tudelft.h5p.com/content/1292050206829217497/embed" aria-label="PA1 File Contents" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://tudelft.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script>

## Question set
This can be used to create a set of Multiple Choice, Drag and Drop, Fill in the blanks, True/False questions and Image choice questions:
<iframe src="https://tudelft.h5p.com/content/1292062157562749767/embed" aria-label="quiz_uniform_motion" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://tudelft.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script>

## True/False Question
<iframe src="https://tudelft.h5p.com/content/1292061623388939257/embed" aria-label="quiz_blue_vs_LS" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe><script src="https://tudelft.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script>