# Overview

This chapter includes examples for jupyter book pages with features. Here is an overview of the different examples:

- H5p: examples of H5p questions
- Combining theory with interactive questions
- Templates for interactive coding / non-coding elements:
    - Programming assignment: Example of how to convert a Jupyter notebook to Jupyter Book with live coding
    - Combining theory with interactive questions and extensive coding assignment: Example how to combine theory, quizzes and code on one page
    - Combining theory with interactive questions: Example how to combine theory with questions, so there's no coding involved.
    - Parametric question: Example for an equation to be checked on equivalence with the correct answer

