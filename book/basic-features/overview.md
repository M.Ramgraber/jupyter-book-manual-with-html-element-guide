# Basic Jupyter Book Features

```{warning} Under Construction!

This page will provide a list of some commonly used book features which are all available in the original jupyter book package. However, this page includes best-practises on how to use them.
```

Things to include somewhere:
- use the right HTML tags for images (described {ref}`here <image-tag-example>`)
- watch out for `png` versus `PNG`
- 2 interesting comments from Quinten about MyST extensions in [this GL Issue](https://gitlab.tudelft.nl/mude/archive-2022/-/issues/25#note_163123)
