# Jupyter Book Manual

A short introduction to Git, VSCode and Jupyter Books for teachers and TA's. Currently, this book only holds some generic installation instructions. In the near future, this will be extended with guides on the Git/Jupyter Book workflow, as well as some recipes and examples on Jupyter Book components. 

## Contributing

Feedback is always welcome. If something doesn't work, or if you have suggestions, you can always make an issue on the Issue Board or make a Merge Request. 
